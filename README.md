# README

I am following the a tutorial series with this git repo in order to setup up a neovim config from scratch.

The playlist for the YouTube series is [here](https://www.youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ)
The github repo is available [here](https://github.com/LunarVim/Neovim-from-scratch/tree/01-options)

This guide is from the creator of LunarVim.

