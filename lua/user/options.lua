-- :help options
vim.opt.backup = false														-- creates a backupfile
vim.opt.clipboard = "unnamedplus"									-- allows neovim to access the system clipboard
vim.opt.cmdheight = 2															-- more space in the neovim command line for displaying messages
vim.opt.completeopt = { "menuone", "noselect" }		-- mostly just for cmp
vim.opt.conceallevel = 0													-- `` is visible in markdown files
vim.opt.fileencoding = "utf-8"										-- encoding for written files
vim.opt.hlsearch = true														-- highlight all matches on previous search pattern
vim.opt.ignorecase = true													-- ignore case in search patterns
vim.opt.mouse = "a"																-- allow the mouse to be used in neovim
vim.opt.pumheight = 10														-- popup menu height
vim.opt.showmode = false													-- don't show -- INSERT -- anymore
vim.opt.showtabline = 2														-- always show tabs
vim.opt.smartcase = true													-- smart case
vim.opt.smartindent = true												-- smart indent
vim.opt.splitbelow = true													-- force all horizontal splits to go below current window
vim.opt.splitright = true													-- force all vertical split to go right of current window
vim.opt.swapfile = false													-- creates a swapfile
vim.opt.termguicolors = true										-- set term gui colors (most terminals support this)
vim.opt.timeoutlen = 1000													-- time to wait for a mapped sequence complete (ms)
vim.opt.undofile = true														-- enable persistent undo
vim.opt.updatetime = 300													-- faster completion (4000 ms default!!!!!)
vim.opt.writebackup = false												-- if a file is being edited by another program it is not allowed to be edited
vim.opt.expandtab = false													-- expandtabs to spaces
vim.opt.shiftwidth = 2														-- number of spaces inserted for each indentation
vim.opt.tabstop = 2																-- insert 2 spaces for a tab
vim.opt.cursorline = false												-- highlight the current line
vim.opt.number = true															-- set numbered lines
vim.opt.relativenumber = false										-- show relative numbered lines
vim.opt.numberwidth = 2														-- set the number column width to 2 (default is 4)
vim.opt.signcolumn = "yes"												-- always show the sign columon
vim.opt.wrap = false															-- word wrap
vim.opt.scrolloff = 8															-- start bump scrolling 8 lines from top/bottom
vim.opt.sidescrolloff = 8													-- start bump scrolling 8 columns from sides
vim.opt.guifont = "monospace:h17"									-- the font used in graphical neovim apps

vim.opt.shortmess:append("c")
vim.opt.iskeyword:append("-")
vim.opt.whichwrap:append("<,>,[,],h,l")
vim.opt.formatoptions:remove("c")
vim.opt.formatoptions:remove("r")
vim.opt.formatoptions:remove("o")
